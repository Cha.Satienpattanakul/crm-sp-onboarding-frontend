# crm-sp-onboarding

Turn on okta.isEnabled=true on default-config.ini in crm-sp-onboarding

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm test
```
