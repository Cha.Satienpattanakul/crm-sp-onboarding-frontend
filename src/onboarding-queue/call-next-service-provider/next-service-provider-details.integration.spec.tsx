import React from 'react';
import '@testing-library/jest-dom';
import NextServiceProviderDetails from './next-service-provider-details.component';
import { fireEvent, render, waitFor, RenderResult } from '@testing-library/react';
import { NextServiceProviderDetail } from '../../shared/interfaces/next-service-provider.interface';

describe('Next Provider Details Integration tests', () => {
  let component : RenderResult;
  beforeEach(() => {
    jest.mock('axios');

    const spDetails: NextServiceProviderDetail = {
      name: 'Test Service Provider',
      companyName: 'Test Company',
      phoneNumber: '555-555-1234',
      rankDescription: 'Testing rank description',
      rank: 1,
      crmSpSummaryUrl: '/link-to-nowhere',
      spId: 123456
    };

    component = render(
      <NextServiceProviderDetails
        nextSp={spDetails}
        getNextSpFunc={() => {}}
        loadingNextSp={false}
        errorLoadingSp={false}
        displaySpLoadError={() => {}}
      />
    );
  });

  afterEach(() => {
    component.unmount();
  });

  it ('should close the dialog for skipping current SP', async () => {    
    const { getByText } = component;

    //When the Details page to display, click on the "Skip current SP" button
    const skipSpButton = getByText('Skip current SP');
    expect(skipSpButton).toBeDefined();
    fireEvent.click(skipSpButton);

    //Wait for the dialog to display, the click the cancel button
    await waitFor(() => {
      expect(getByText('Confirm Skipping Current Service Provider')).toBeVisible();
    });
    fireEvent.click(getByText('Cancel'));

    //Verify that the dialog no longer displays
    await waitFor(() => {
      expect(getByText('Confirm Skipping Current Service Provider')).not.toBeVisible();
    });
  });

  it ('should get next SP when confirming skipping current SP', async () => {
    const skipSpId = 12345;
    const { getByText, getByTestId } = component;
    const contactNameElement = getByTestId('contact-name');
    const firstSpName = contactNameElement.textContent
    expect(firstSpName).not.toBe('');

    //When the Details page displays, click on the "Skip current SP" button
    const skipSpButton = getByText('Skip current SP');
    expect(skipSpButton).toBeDefined();
    fireEvent.click(skipSpButton);

    //Wait for the dialog to display, the click button to skip the current SP
    await waitFor(() => {
      expect(getByText('Confirm Skipping Current Service Provider')).toBeVisible();
    });
    const confirmSkipButton = getByText('Confirm');
    expect(confirmSkipButton).toBeDefined();

    //Mock the get and post requests, click the confirm button
    fireEvent.click(confirmSkipButton);  

    //Verify that the dialog no longer displays
    await waitFor(() => {
      expect(getByText('Confirm Skipping Current Service Provider')).not.toBeVisible();
    });
  });
});