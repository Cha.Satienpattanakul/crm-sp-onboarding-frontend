export interface CardActionButtonsProps {
    pullBtnEnabled: boolean;
    skipDisabled: boolean;
    onGoToSpSummaryClick: () => void;
    onGetNextSpClick: () => void;
    onSkipSpClick: () => void;
}
