import { Button } from '@mui/material';
import React from 'react';
import { CardActionButtonsProps } from './card-action-buttons.interface';

const CardActionButtons: React.FC<CardActionButtonsProps> = props => {
    return (
        <>
            {props.pullBtnEnabled ? (
                <Button size="small" onClick={props.onGetNextSpClick}>Pull next SP</Button>
            ) : (
                <>
                    <Button size="small" onClick={props.onGoToSpSummaryClick}>Go to Sp Summary</Button>
                    {props.skipDisabled ? (
                        <></>
                    ) : (
                        <Button size="small" onClick={props.onSkipSpClick}>Skip current SP</Button>
                    )}
                </>
            )}
        </>
    );
};

export default CardActionButtons;
