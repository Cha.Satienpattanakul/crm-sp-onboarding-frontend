import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import CardActionButtons from './card-action-buttons.component';
import '@testing-library/jest-dom'

describe('Test CardActionButtons and its props', () => {
    let component: RenderResult;
    const onGetNextSpClick = () => {};
    const onGoToSpSummaryClick = () => {};
    const onSkipSpClick = () => {};
    describe('Render the component and show two buttons', () => {
        beforeEach(() => {
            component = render(
                <CardActionButtons
                    pullBtnEnabled={false}
                    skipDisabled={false}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });

        it('should display two buttons', async () => {
            const skipButon = await component.findByText('Skip current SP');
            expect(skipButon).toBeVisible();
            const goToSpButton = await component.findByText('Go to Sp Summary');
            expect(goToSpButton).toBeVisible();
        });

    });
    describe('Render the component and hide "Skip current SP" for rank 0', () => {
        beforeEach(() => {
            component = render(
                <CardActionButtons
                    pullBtnEnabled={false}
                    skipDisabled={true}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('should display "Go to Sp Summary" button only', async () => {
            const skipButton = await component.queryAllByText('Skip current SP');
            expect(skipButton).toHaveLength(0);
            const goToSpButton = await component.findByText('Go to Sp Summary');
            expect(goToSpButton).toBeVisible();
        });
    });
    describe('Render the component and only show the pull next sp button', () => {
        beforeEach(() => {
            component = render(
                <CardActionButtons
                    pullBtnEnabled={true}
                    skipDisabled={false}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });

        it('should display one button', async () => {
            const pullButton = await component.findByText('Pull next SP');
            expect(pullButton).toBeVisible();
        });
    });
});
