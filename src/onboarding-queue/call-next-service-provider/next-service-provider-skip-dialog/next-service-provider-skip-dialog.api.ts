import request from '../../../shared/requests/request.api';

const baseUrl = '/api/sp-onboarding/priority-queue/skip-sp/';

export const skipServiceProvider = async (spId: number) : Promise<boolean> => {
    try {
        const response = await request.post(`${baseUrl}${spId}`);
        return response.status === 200;
    } catch(error) {
        return false;
    }
}