interface INextServiceProviderDialogProps {
    open: boolean;
    name: string;
    spId: number;
    onClose: () => void;
    loadNextSp: () => void;
    displaySpLoadError: () => void;
}

export default INextServiceProviderDialogProps;