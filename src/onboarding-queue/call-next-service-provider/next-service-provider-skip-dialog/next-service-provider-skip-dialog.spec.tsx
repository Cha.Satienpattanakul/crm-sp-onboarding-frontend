import React from 'react';
import '@testing-library/jest-dom';
import { RenderResult, render } from '@testing-library/react';
import NextServiceProviderSkipDialog from './next-service-provider-skip-dialog.component';
import INextServiceProviderDialogProps from './next-service-provider-skip-dialog.interface';

describe('Testing NextServiceProviderSkipDialog', () => {
    let component: RenderResult;
    const props: INextServiceProviderDialogProps = {
        open: true,
        name: "Testing SP Name",
        spId: 12345,
        onClose: () => {},
        loadNextSp: () => {},
        displaySpLoadError: () => {}
    }
    beforeEach(() => {
        component = render(
            <NextServiceProviderSkipDialog
                open={props.open}
                name={props.name}
                spId={props.spId}
                onClose={props.onClose}
                loadNextSp={props.loadNextSp}
                displaySpLoadError={props.displaySpLoadError}
            />
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it('Should display successfully', async () => {
        const content = await component.findByText('Confirm Skipping Current Service Provider');
        expect(content).toBeVisible();
    });
});