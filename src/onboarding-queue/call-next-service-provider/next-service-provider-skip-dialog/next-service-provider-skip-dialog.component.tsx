import React from 'react'
import {Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Divider} from '@mui/material';
import INextServiceProviderDialogProps from './next-service-provider-skip-dialog.interface'; 
import { skipServiceProvider } from './next-service-provider-skip-dialog.api'

const NextServiceProviderSkipDialog : React.FC<INextServiceProviderDialogProps> = props => {

    const confirmSkipSp = async () => {
        const success = await skipServiceProvider(props.spId);
        props.onClose();
        if(success) {
            props.loadNextSp();
        } else {
            props.displaySpLoadError();
        }
    }

    return(
        <>
            <Dialog open={props.open} onClose={props.onClose}>
                <DialogTitle>Confirm Skipping Current Service Provider</DialogTitle>
                <Divider variant='middle' />
                <DialogContent>
                    <DialogContentText>
                        Are you sure you want to skip the current Service Provider "<strong>{props.name}</strong>"?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        size="small"
                        onClick={props.onClose}
                        variant="outlined"
                    >
                        Cancel
                    </Button>
                    <Button
                        size="small"
                        onClick={confirmSkipSp}
                        variant="contained"
                    >
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default NextServiceProviderSkipDialog;