import { NextServiceProviderDetail } from "../../shared/interfaces/next-service-provider.interface";

export interface NextServiceProviderDetailsProps {
    nextSp: NextServiceProviderDetail;
    loadingNextSp: boolean;
    errorLoadingSp: boolean;
    getNextSpFunc: () => void;
    displaySpLoadError: () => void;
}
