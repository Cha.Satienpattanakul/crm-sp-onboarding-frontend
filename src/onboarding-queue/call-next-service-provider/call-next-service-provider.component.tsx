import React, { FC, useState, useEffect } from 'react';
import { Grid, CircularProgress, Alert } from '@mui/material';
import NextServiceProviderDetails from './next-service-provider-details.component';
import NextServiceProviderCsmDetails from './next-service-provider-csm-details/next-service-provider-csm-details.component';
import ErrorAlert from "./alert/alert.component";
import { getNextServiceProvider } from '../assignments/assignment.interface';
import {
  CsmDetails,
  NextServiceProviderDetail,
} from '../../shared/interfaces/next-service-provider.interface';

const CallNextServiceProvider: FC = () => {
  const [loading, setLoading] = useState(true);
  const [nextSpDetails, setNextSpDetails] = useState(
    {} as NextServiceProviderDetail
  );
  const [nextSpCsmDetails, setNextSpCsmDetails] = useState({} as CsmDetails);
  const [loadingNextSp, setLoadingNextSp] = useState(true);
  const [errorLoadingSp, setErrorLoadingSp] = useState(false);

  const loadNextSp = async () => {
    setLoadingNextSp(true);
    setErrorLoadingSp(false);
    try {
      const result = await getNextServiceProvider();
      if (Object.keys(result).length === 0) {
        setNextSpDetails({} as NextServiceProviderDetail);
        setLoadingNextSp(false);
        setLoading(false)
        return
      }
      setNextSpDetails(result.nextSpDetails);
      setNextSpCsmDetails(result.csmDetails);
      setLoading(false);
    } catch (error) {
      setNextSpDetails({} as NextServiceProviderDetail);
      setNextSpCsmDetails({} as CsmDetails);
      setLoading(false);
      displaySpLoadError();
    }
    setLoadingNextSp(false);
  };

  const displaySpLoadError = () => {
    setErrorLoadingSp(true);
  };

  // Runs once, after mounting
  useEffect(() => {
    loadNextSp();
    return () => {
      setNextSpDetails({} as NextServiceProviderDetail);
      setNextSpCsmDetails({} as CsmDetails);
    };
  }, []);

  function hasCsmDetails() {
    return nextSpCsmDetails && Object.keys(nextSpCsmDetails).length !== 0;
  }

  function hasSpDetails() {
    return nextSpDetails && Object.keys(nextSpDetails).length !== 0;
  }

  return (
    <>
      {loading ? (
        <CircularProgress
          style={{ position: 'absolute', left: '50%', top: '50%' }}
        />
      ) : (
        <>
          { !hasSpDetails() && !errorLoadingSp &&
              <Alert sx={{mb:2}} severity="warning">
                Nothing in the Queue. Try again later.
              </Alert>
          }
          <ErrorAlert
            show={errorLoadingSp}
            message="Error in connection!"
          />
          <Grid item xs={6}>
            <NextServiceProviderDetails
              nextSp={nextSpDetails}
              getNextSpFunc={loadNextSp}
              loadingNextSp={loadingNextSp}
              errorLoadingSp={errorLoadingSp}
              displaySpLoadError={displaySpLoadError}
            />
          </Grid>
          <Grid item xs={6} marginTop={5}>
            {hasCsmDetails() && (
              <NextServiceProviderCsmDetails
                csmDetails={nextSpCsmDetails}
                loadingCsmDetails={false}
                errorLoadingCsm={false}
              />
            )}
          </Grid>
        </>
      )}
    </>
  );
};

export default CallNextServiceProvider;
