import React from 'react';
import NextSpDetailsCard from './next-service-provider-details-content/next-service-provider-details-content.component';
import { NextServiceProviderDetailsProps } from './next-service-provider-details.interface';
import NextSpDetailsSkeleton from './next-service-provider-details-skeleton/next-service-provider-details-skeleton.component';
import NextServiceProviderSkipDialog from './next-service-provider-skip-dialog/next-service-provider-skip-dialog.component';

const NextSpDetailsComponent = React.memo<NextServiceProviderDetailsProps>(
  (props) => {
    const [isSpSummaryButtonVisible, setIsSpSummaryButtonVisible] =
      React.useState(true);
    const [isSkipPromptVisible, setIsSkipPromptVisible] = React.useState(false);

    const loadSpSummary = () => {
      window.open(props.nextSp.crmSpSummaryUrl, 'SpSummary');
      setIsSpSummaryButtonVisible(false);
    };

    const getNextSpClick = () => {
      props.getNextSpFunc();
      setIsSpSummaryButtonVisible(true);
    };

    const displaySkipSpDialog = () => {
      setIsSkipPromptVisible(true);
    };

    const hideSkipSpDialog = () => {
      setIsSkipPromptVisible(false);
    };

    return props.loadingNextSp ? (
      <NextSpDetailsSkeleton {...props} />
    ) : (
      <>
        <NextSpDetailsCard
          name={props.nextSp.name}
          companyName={props.nextSp.companyName}
          phoneNumber={props.nextSp.phoneNumber}
          rankDescription={props.nextSp.rankDescription}
          rank={props.nextSp.rank}
          skipDisabled={props.nextSp.rank == 0}
          pullBtnEnabled={
            !isSpSummaryButtonVisible ||
            props.loadingNextSp ||
            props.errorLoadingSp
          }
          onGetNextSpClick={getNextSpClick}
          onGoToSpSummaryClick={loadSpSummary}
          onSkipSpClick={displaySkipSpDialog}
        />
        <NextServiceProviderSkipDialog
          open={isSkipPromptVisible}
          name={props.nextSp.name}
          spId={props.nextSp.spId}
          onClose={hideSkipSpDialog}
          loadNextSp={props.getNextSpFunc}
          displaySpLoadError={props.displaySpLoadError}
        />
      </>
    );
  }
);

NextSpDetailsComponent.displayName = 'NextSpDetailsComponent';

export default NextSpDetailsComponent;
