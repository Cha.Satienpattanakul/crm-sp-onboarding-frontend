import React from 'react';
import '@testing-library/jest-dom';
import { RenderResult, render } from '@testing-library/react';
import NextServiceProviderCsmDetails from './next-service-provider-csm-details.component';

describe('Testing NextServiceProviderCsmDetails', () => {
    let component: RenderResult;
    const firstName = 'test first name';
    const lastName = 'test last name';
    const phoneNumber = '0000000000';
    beforeEach(() => {
        component = render(
            <NextServiceProviderCsmDetails
                csmDetails={{
                    firstName,
                    lastName,
                    phoneNumber
                }}
                loadingCsmDetails={false}
                errorLoadingCsm={false}
            />
        );
    });
    afterEach(() => {
        component.unmount();
    });

    it('should display the props correctly', async () => {
        const firstNameEl = await component.getByText(firstName);
        expect(firstNameEl).toBeVisible();
        const lastNameEl = await component.getByText(lastName);
        expect(lastNameEl).toBeVisible();
        const phoneEl = await component.getByText('(000) 000-0000');
        expect(phoneEl).toBeVisible();
    });
});
