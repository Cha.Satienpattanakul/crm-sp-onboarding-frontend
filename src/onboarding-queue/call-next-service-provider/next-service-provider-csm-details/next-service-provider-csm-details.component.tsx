import React, {FC} from "react";
import {Grid, CardHeader, Divider} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { formattedPhoneNumber } from "../../../utils/phone-number.format";
import { CsmDetails } from "../../../shared/interfaces/next-service-provider.interface";


interface Props {
    csmDetails: CsmDetails,
    loadingCsmDetails: boolean,
    errorLoadingCsm: boolean
}
const NextServiceProviderCsmDetails: FC<Props> = ({ csmDetails }) => {
    const primaryColor = "text.primary";
    const secondaryColor = "text.secondary";

    return (
        <>
            <Card sx={{ minWidth: 275 }}>
                <CardHeader title='CSM Details' />
                <Divider variant='middle' />
                <CardContent>
                    <Grid container spacing={1}>
                        <Grid item xs={2}>
                            <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color={primaryColor} align='left'>
                                First Name:
                            </Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <Typography sx={{ mb: 1.5 }} color={secondaryColor} align='left'>
                                {csmDetails.firstName}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color={primaryColor} align='left'>
                                Last Name:
                            </Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <Typography sx={{ mb: 1.5 }} color={secondaryColor} align='left'>
                                {csmDetails.lastName}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color={primaryColor} align='left'>
                                Phone Number:
                            </Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <Typography sx={{ mb: 1.5 }} color={secondaryColor} align='left'>
                                {formattedPhoneNumber(csmDetails.phoneNumber)}
                            </Typography>
                        </Grid>

                    </Grid>
                </CardContent>
            </Card>
        </>
    )
};

export default NextServiceProviderCsmDetails;
