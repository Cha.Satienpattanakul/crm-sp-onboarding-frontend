import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import Alert from './alert.component';
import '@testing-library/jest-dom'

describe('Test alert component and its props', () => {
    const dontShow = false;
    const shouldShow = true;
    const message = 'Hello world';
    
    describe('Component should be visible', () => {
        let component: RenderResult;
        beforeEach(() => {
            component = render(
                <Alert message={message} show={shouldShow} />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('Shold display the Alert and its message', async () => {
            const messageAlert = await component.findAllByText(message);
            expect(messageAlert[0]).toBeInTheDocument();
        });
    });
    describe('Component should not be visible', () => {
        let component: RenderResult;
        beforeEach(() => {
            component = render(
                <Alert message={message} show={dontShow} />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('Shold not display the Alert', async () => {
            expect(document.body.children[0]).toBeEmptyDOMElement();
        });
    });
});
