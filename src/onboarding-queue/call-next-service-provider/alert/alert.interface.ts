export interface AlertProps {
    message: string;
    show: boolean;
}
