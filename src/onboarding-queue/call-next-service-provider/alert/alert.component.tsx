import React from 'react';
import { AlertProps } from './alert.interface';
import { Alert as ErrorAlert } from "@mui/material";

const Alert = React.memo<AlertProps>(props => {
    return (
        <>
            {props.show && (
                <ErrorAlert severity='error' sx={{mb: 3}}>{props.message}</ErrorAlert>
            )}
        </>
    );
});

Alert.displayName = 'Alert';

export default Alert;
