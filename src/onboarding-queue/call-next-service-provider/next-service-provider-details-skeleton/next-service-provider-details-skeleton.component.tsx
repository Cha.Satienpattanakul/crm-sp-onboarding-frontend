import React from 'react';
import { Skeleton } from '@mui/material';
import NextServiceProviderDetailsCard from '../next-service-provider-details-content/next-service-provider-details-content.component';
import { NextServiceProviderDetailsProps } from '../next-service-provider-details.interface';

const NextServiceProviderDetailsSkeleton: React.FC<NextServiceProviderDetailsProps> = props => {
    return (
        <>
            <Skeleton data-testid="nextSpSkeletonTest">
                <NextServiceProviderDetailsCard
                name={props.nextSp.name}
                companyName={props.nextSp.companyName}
                phoneNumber={props.nextSp.phoneNumber}
                rankDescription={props.nextSp.rankDescription}
                rank={props.nextSp.rank}
                skipDisabled={props.nextSp.rank == 0}
                pullBtnEnabled={props.loadingNextSp || props.errorLoadingSp}
                onGetNextSpClick={props.getNextSpFunc}
                onGoToSpSummaryClick={() => {}}
                onSkipSpClick={() => {}}
                />
            </Skeleton>
        </>
    );
};

export default NextServiceProviderDetailsSkeleton;
