import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import NextSpDetailsSkeleton from './next-service-provider-details-skeleton.component';
import '@testing-library/jest-dom';
import { NextServiceProviderDetail } from '../../../shared/interfaces/next-service-provider.interface';

describe('Testing NextSpDetailsSkeleton', () => {
    let component: RenderResult;
    beforeEach(() => {
        component = render(
            <NextSpDetailsSkeleton
                nextSp={{} as NextServiceProviderDetail}
                getNextSpFunc={() => {}}
                loadingNextSp={false}
                errorLoadingSp={false}
                displaySpLoadError={() => {}}
            />
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it ('Should display succesfull', async () => {
        const content = await component.getByTestId('nextSpSkeletonTest');
        expect(content).toBeVisible();
    });
});
