export interface NextServiceProviderDetailsContentProps {
    name: string;
    companyName: string;
    phoneNumber: string;
    rank: number;
    rankDescription: string;
    pullBtnEnabled: boolean;
    skipDisabled: boolean;
    onGoToSpSummaryClick: () => void;
    onGetNextSpClick: () => void;
    onSkipSpClick: () => void;
}
