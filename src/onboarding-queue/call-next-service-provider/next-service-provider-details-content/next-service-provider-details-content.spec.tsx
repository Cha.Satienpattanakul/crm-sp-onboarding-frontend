import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import NextServiceProviderDetailsCard from './next-service-provider-details-content.component';
import '@testing-library/jest-dom';

describe('Testing NextSpDetailsCard', () => {
    let component: RenderResult;
    const name = 'Test name';
    const companyName = 'Test company name';
    const phoneNumber = '0000000000';
    const rankDescription = 'Test description';
    const rank = 1;
    const onGetNextSpClick = () => {};
    const onGoToSpSummaryClick = () => {};
    const onSkipSpClick = () => {};
    describe('test the component is displaying the data correctly and display two buttons', () => {
        beforeEach(() => {
            component = render(
                <NextServiceProviderDetailsCard
                    name={name}
                    companyName={companyName}
                    phoneNumber={phoneNumber}
                    rank={rank}
                    rankDescription={rankDescription}
                    skipDisabled={false}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                    pullBtnEnabled={false}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });

        it('Should display the data passed by props', async () => {
            const nameEl = await component.findByText(name);
            expect(nameEl).toBeVisible();
            const companyEl = await component.findByText(companyName);
            expect(companyEl).toBeVisible();
            const phoneEl = await component.findByText('(000) 000-0000');
            expect(phoneEl).toBeVisible();
            const rankEl = await component.findByText(rankDescription);
            expect(rankEl).toBeVisible();
        });
        it('Should display two buttons', async () => {
            const skipButon = await component.findByText('Skip current SP');
            expect(skipButon).toBeVisible();
            const goToSpButton = await component.findByText('Go to Sp Summary');
            expect(goToSpButton).toBeVisible();
            const pullButton = await component.queryAllByText('Pull next SP');
            expect(pullButton).toHaveLength(0);
        });
    });
    describe('test the component is displaying one button', () => {
        beforeEach(() => {
            component = render(
                <NextServiceProviderDetailsCard
                    name={name}
                    companyName={companyName}
                    phoneNumber={phoneNumber}
                    rank={rank}
                    rankDescription={rankDescription}
                    skipDisabled={false}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                    pullBtnEnabled={true}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('Should display one button', async () => {
            const skipButon = await component.queryAllByText('Skip current SP');
            expect(skipButon).toHaveLength(0);
            const goToSpButton = await component.queryAllByText('Go to Sp Summary');
            expect(goToSpButton).toHaveLength(0);
            const pullButton = await component.findByText('Pull next SP');
            expect(pullButton).toBeVisible();
        });
    });
    describe('test the component does not display "Skip Current SP" button when skipDisabled is TRUE', () => {
        beforeEach(() => {
            component = render(
                <NextServiceProviderDetailsCard
                    name={name}
                    companyName={companyName}
                    phoneNumber={phoneNumber}
                    rank={rank}
                    rankDescription={rankDescription}
                    skipDisabled={true}
                    onGetNextSpClick={onGetNextSpClick}
                    onGoToSpSummaryClick={onGoToSpSummaryClick}
                    onSkipSpClick={onSkipSpClick}
                    pullBtnEnabled={false}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('The button to skip the current SP should not display', async () => {
            const skipButton = await component.queryAllByText('Skip current SP');
            expect(skipButton).toHaveLength(0);
            const goToSpButton = await component.queryAllByText('Go to Sp Summary');
            expect(goToSpButton).toHaveLength(1);
            const pullButton = await component.queryAllByText('Pull next SP');
            expect(pullButton).toHaveLength(0);
        });
    });
});
