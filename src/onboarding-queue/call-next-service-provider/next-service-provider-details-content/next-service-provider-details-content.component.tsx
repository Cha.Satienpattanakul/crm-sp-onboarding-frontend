import React from 'react';
import { NextServiceProviderDetailsContentProps } from './next-service-provider-details-content.interface';
import { Grid, Typography, Card, CardHeader, CardContent, CardActions, Divider } from '@mui/material';
import CardActionButtons from '../card-action-buttons/card-action-buttons.component';
import { formattedPhoneNumber } from '../../../utils/phone-number.format';

const NextServiceProviderDetailsCard: React.FC<NextServiceProviderDetailsContentProps> = props => {
    return (
        <>
            <Card sx={{ minWidth: 275 }}>
                <CardHeader title='Service Provider Details' />
                <Divider variant='middle' />
                <CardContent>
                    <Grid container spacing={1}>
                        <Grid item xs={2}>
                        <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color="text.primary" align='left'>
                            Contact Name:
                        </Typography>
                        </Grid>
                        <Grid item xs={10}>
                        <Typography sx={{ mb: 1.5 }} color="text.secondary" align='left' data-testid="contact-name">
                            {props.name}
                        </Typography>
                        </Grid>
                        <Grid item xs={2}>
                        <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color="text.primary" align='left'>
                            Company Name:
                        </Typography>
                        </Grid>
                        <Grid item xs={10}>
                        <Typography sx={{ mb: 1.5 }} color="text.secondary" align='left'>
                            {props.companyName}
                        </Typography>
                        </Grid>
                        <Grid item xs={2}>
                        <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color="text.primary" align='left'>
                            Phone Number:
                        </Typography>
                        </Grid>
                        <Grid item xs={10}>
                        <Typography sx={{ mb: 1.5 }} color="text.secondary" align='left'>
                            {formattedPhoneNumber(props.phoneNumber)}
                        </Typography>
                        </Grid>
                        <Grid item xs={2}>
                        <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color="text.primary" align='left'>
                            Reason For Call:
                        </Typography>
                        </Grid>
                        <Grid item xs={10}>
                        <Typography sx={{ mb: 1.5 }} color="text.secondary" align='left'>
                            {props.rankDescription}
                        </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography sx={{ mb: 1.5, fontWeight: 'bold' }} color="text.primary" align='left'>
                                Rank:
                            </Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary" align='left'>
                                {props.rank}
                            </Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    <CardActionButtons
                        onGetNextSpClick={props.onGetNextSpClick}
                        onGoToSpSummaryClick={props.onGoToSpSummaryClick}
                        onSkipSpClick={props.onSkipSpClick}
                        pullBtnEnabled={props.pullBtnEnabled}
                        skipDisabled={props.skipDisabled}
                    />
                </CardActions>
            </Card>
        </>
    );
};

export default NextServiceProviderDetailsCard;
