import { AllSpQueueItemVo } from "./onboarding-queue-assignment.interface";
import { NextServiceProviderResponse } from "../../shared";
import request from "../../shared/requests/request.api";

const baseUrl = '/api/sp-onboarding/priority-queue/next-sp';

export async function getNextServiceProvider(): Promise<NextServiceProviderResponse> {
    const response = await request.get(`${baseUrl}`);
    return response.data;
}

export async function getAssignments(onboardingRepId: number): Promise<AllSpQueueItemVo[]> {
    const response = await request.get(`${baseUrl}/${onboardingRepId}`);
    return response.data as AllSpQueueItemVo[];
}