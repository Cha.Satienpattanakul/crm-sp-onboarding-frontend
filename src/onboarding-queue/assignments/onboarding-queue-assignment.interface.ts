export interface AllSpQueueItemVo {
  queueId: number;
  spId: number;
  rank: number;
  rankDescription: string;
  timeZone: string;
  lastCallDateTime: string;
  skipDateTime: string;
  name: string;
  companyName: string;
  phoneNumber: string;
}

export const convertDateTime = (dateTimeString: string) => {
  const date = new Date(dateTimeString)
    .toLocaleString('en-US', { timeZone: 'America/Denver', hour12: true })
    .replace(', ', ' ');
  return date + ' MT';
};
