import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { act } from 'react-dom/test-utils';
import PriorityQueueAssignments from './onboarding-queue-assignment.component';

beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
    jest.spyOn(console, 'error').mockImplementation(() => {});
});

describe('Testing PriorityQueueAssignments', () => {
    let component: RenderResult;
    beforeEach(() => {
        component = render(
            <PriorityQueueAssignments drawerWidth={0} drawerOpen={false} toggleDrawer={function (): void {
                throw new Error('Function not implemented.');
            } } />
        );
    });
    afterEach(() => {
        component.unmount();
    });
    it('should display the table', () => {
        act(async () => {
            const table = component.getByText('Priority Queue Assignments');
            expect(table).toBeInTheDocument();
        });
    });
    it('should have a select', async () => {
        const select = await component.findByTestId('onboardingRep-select');
        act(() => {
            select.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        });
    });
});