import { Alert, Box } from '@mui/material';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import React, { FC } from 'react';
import {AllSpQueueItemVo, convertDateTime} from './onboarding-queue-assignment.interface';
import './styles.scss';
import MaterialTable from '@material-table/core';
import { TitleProps } from '../../shared/interfaces';
import { OnboardingRepresentative } from '../../onboarding-rep/onboarding-representative';
import { getOnboardingRep } from '../../onboarding-rep/onboarding-rep.api';
import { getAssignments } from './assignment.interface';
import { formattedPhoneNumber } from '../../utils/phone-number.format';

const PriorityQueueAssignments: FC<TitleProps> = ({drawerOpen, drawerWidth, toggleDrawer}) => {
  const [onboardingRepId, setOnboardingRepId] = React.useState('');
  const [name, setName] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [errorLoading, setErrorLoading] = React.useState(false);
  const [priorityQueue, setPriorityQueue] = React.useState<AllSpQueueItemVo[]>([]);
  const [onboardingRep, setOnboardingRep] = React.useState<OnboardingRepresentative[]>([]);

  const loadOnboardingRepresentative = async () => {
    try {
      setLoading(true);
      const onboardingRepresentativeList: OnboardingRepresentative[] = await getOnboardingRep();
      setOnboardingRep(onboardingRepresentativeList);
    }
    catch {
      setOnboardingRep([]);
      setErrorLoading(true);
    }
    finally {
      setLoading(false);
    }
  };

  const loadPriorityQueueAssignments = async () => {
    try {
      setLoading(true);
      const priorityQueueList: AllSpQueueItemVo[] = await getAssignments(parseInt(onboardingRepId));
      priorityQueueList.map((queue) => {
        if (queue.lastCallDateTime) queue.lastCallDateTime = convertDateTime(queue.lastCallDateTime);
        if (queue.skipDateTime) queue.skipDateTime = convertDateTime(queue.skipDateTime);
        if (queue.phoneNumber) queue.phoneNumber = formattedPhoneNumber(queue.phoneNumber);
      });
      setPriorityQueue(priorityQueueList);
    } catch (e) {
      setPriorityQueue([]);
      setErrorLoading(true);
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    if (!onboardingRepId) {
      loadOnboardingRepresentative();
    } else {
      loadPriorityQueueAssignments();
    }
    return () => {
      setPriorityQueue([]);
      setErrorLoading(false);
    };
  }, [onboardingRepId]);

  const sortOnboardingRepByName = (a: OnboardingRepresentative, b: OnboardingRepresentative) => {
    var c = a.name.toLowerCase();
    var d = b.name.toLowerCase();
    if (c < d) {
      return -1;
    }
    if (c > d) {
      return 1;
    }
    return 0;
  };

  return (
    <div className="rank-select--container">
      {errorLoading && (
        <Alert sx={{mb:4}} severity="error">
          Something went wrong. Please try again or contact the development team.
        </Alert>
      )}
      <Box sx={{ maxWidth: 'auto' }}>
        <Autocomplete
          isOptionEqualToValue={(option, value) => option !== value}
          disablePortal
          id="combo-box-demo"
          data-testid="onboardingRep-select"
          options={onboardingRep
            .sort(sortOnboardingRepByName)
            .map((item) => item.name)}
          sx={{ width: 300, marginBottom: 2 }}
          renderInput={(params) => (
            <TextField {...params} label="Onboarding Representative" />
          )}
          value={name as string}
          onChange={(event: any, newValue: string | null) => {
            setName(newValue!);
            const rep = onboardingRep.find((item) => item.name === newValue);
            setOnboardingRepId(rep ? rep.id.toString() : '');
          }}
          defaultValue=""
        />
        <MaterialTable
          columns={[
            { title: 'SP Id', field: 'spId', id: 'spId', filtering: false },
            { title: 'Name', field: 'name', id: 'name', filtering: false },
            {
              title: 'Company Name',
              field: 'companyName',
              id: 'companyName',
              filtering: false,
            },
            {
              title: 'Phone #',
              field: 'phoneNumber',
              id: 'phoneNumber',
              filtering: false,
            },
            {
              title: 'Rank #',
              field: 'rank',
              id: 'rank',
              filtering: false,
              width: '10%',
            },
            {
              title: 'Time Zone',
              field: 'timeZone',
              id: 'timeZone',
              filtering: false,
              width: '10%',
            },
            {
              title: 'Last Call',
              field: 'lastCallDateTime',
              id: 'lastCallDateTime',
              type: 'datetime',
              filtering: false,
            },
            {
              title: 'Skip Date',
              field: 'skipDateTime',
              id: 'skipDateTime',
              type: 'datetime',
              filtering: false,
            },
          ]}
          data={priorityQueue}
          title="Priority Queue Assignments"
          options={{
            filtering: false,
            grouping: true,
            rowStyle: { textAlign: 'left' },
            filterRowStyle: { textAlign: 'left' },
            editCellStyle: { textAlign: 'left' },
            pageSize: 10,
            pageSizeOptions: [10, 20, 30],
            tableLayout: 'fixed',
          }}
          isLoading={loading}
        />
      </Box>
    </div>
  );
};

export default PriorityQueueAssignments;
