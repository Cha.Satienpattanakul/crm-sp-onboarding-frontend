import { Grid } from '@mui/material';
import { powerHour } from './power-hour.api';
import React from 'react';
import { Rank, RawServiceProvidersList } from './power-hour.interface';
import { PowerHourServiceProvider } from './service-provider-table/service-provider-table.interface';
import ServiceProviderTable from './service-provider-table/service-provider-table.component';
import { getServiceProviderRanks } from '../ranks.api';
import RankSelect from './rank-select/rank-select.component';
import { formattedPhoneNumber } from '../../utils/phone-number.format';

const PowerHourContainer = React.memo(() => {
  const [serviceProviders, setServiceProviders] = React.useState<PowerHourServiceProvider[]>([]);
  const [unfilteredServiceProviders, setUnfilteredServiceProviders] = React.useState<PowerHourServiceProvider[]>([]);
  const [loading, setLoading] = React.useState(true);
  const [ranks, setRanks] = React.useState<Rank[]>([]);
  const [rank, setRank] = React.useState(1);

  const setPowerHourSps = async () => {
    try {
      setLoading(true);
      const serviceProvidersRawList: RawServiceProvidersList[] = await powerHour();
      const serviceProviderList: PowerHourServiceProvider[] = [];
      serviceProvidersRawList.forEach((item) => {
        const serviceProvider: PowerHourServiceProvider = {} as PowerHourServiceProvider;
        serviceProvider.callRate = item.callRate;
        serviceProvider.companyName = item.companyName;
        serviceProvider.lastContactedDateTime = item.lastContactedDateTime;
        serviceProvider.enrollmentStatus = item.enrollmentStatus.enrollmentStatus;
        serviceProvider.name = item.name;
        serviceProvider.numberOfRatings = item.numberOfRatings;
        serviceProvider.phone = formattedPhoneNumber(item.phoneNumber);
        serviceProvider.rank = item.rank;
        serviceProvider.spId = item.spId;
        serviceProvider.rankDescription = item.rankDescription;
        serviceProvider.crmSpSummaryUrl = item.crmSpSummaryUrl;
        serviceProviderList.push(serviceProvider);
      });
      setServiceProviders(serviceProviderList);
      setUnfilteredServiceProviders(serviceProviderList)
    }catch (error){
      setLoading(false)
    }
  };
  const setSpRanks = async () => {
    try {
      const spRanks = await getServiceProviderRanks();
      setRanks(spRanks.ranks);
    }catch (error){
      setRanks([])
    }
  };
  React.useEffect(() => {
    setPowerHourSps().then(() => {
      setLoading(false)
    });
    setSpRanks()
    return () => {
      setRanks([]);
      setServiceProviders([]);
    };
  }, []);

  const onChange = (newRank: number) => {
    setLoading(true)
    setRank(newRank);
    const filteredServiceProviders = unfilteredServiceProviders.filter(sp => sp.rank === newRank)
    setServiceProviders(filteredServiceProviders)
    setTimeout(()=>{
      setLoading(false)
    }, 1000)
  };

  return (
    <Grid
      data-testid="pwc-container"
      container
      justifyContent="center"
      alignItems="center"
    >
      <Grid item xs={12}>
        <RankSelect onChange={onChange} value={rank} ranks={ranks} />
      </Grid>
      <Grid item xs={12}>
        <ServiceProviderTable data={serviceProviders} loading={loading} />
      </Grid>
    </Grid>
  );
});

PowerHourContainer.displayName = 'PowerHourContainer';

export default PowerHourContainer;
