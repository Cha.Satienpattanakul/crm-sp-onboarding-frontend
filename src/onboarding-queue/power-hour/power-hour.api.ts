import request from "../../shared/requests/request.api";
import { RawServiceProvidersList } from "./power-hour.interface";

const baseUrl = '/api/sp-onboarding/priority-queue/power-hour';

export async function powerHour(): Promise<RawServiceProvidersList[]> {
    try {
        const response = await request.get(baseUrl);
        return response.data;
    } catch (error) {
        return [];
    }
}
