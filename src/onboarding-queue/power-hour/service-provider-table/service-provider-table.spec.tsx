import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import ServiceProviderTable from './service-provider-table.component';

beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {});
});

describe('Testing the SpTable', () => {
    let component: RenderResult;
    const name = 'Carlos';
    const enrollmentStatus = 'Hold License';
    const numberOfRatings = 5;
    const phone = '5555555555';
    const companyName = 'Home Depot';
    const callRate = 2;
    const spId = 20;
    const lastContactedDateTime = '11-23-2021 12:00:00 AM MT';
    const rank = 1;
    const rankDescription = 'Test'
    const crmSpSummaryUrl = '#';
    describe('SpTable with data', () => {
        beforeEach(() => {
            component = render(
                <ServiceProviderTable
                    data={[{ 
                        name,
                        enrollmentStatus,
                        numberOfRatings,
                        phone,
                        companyName,
                        callRate,
                        spId,
                        lastContactedDateTime,
                        rank,
                        rankDescription,
                        crmSpSummaryUrl
                    }]}
                />
            );
        });
        afterEach(() => {
            component.unmount();
        });
        it('should display the data passed by props correctly', async () => {
            const nameEl = await component.findByText(name);
            expect(nameEl).toBeVisible();
            const enrollmentEl = await component.findByText(enrollmentStatus);
            expect(enrollmentEl).toBeVisible();
            const ratingsEl = await component.findByText(numberOfRatings);
            expect(ratingsEl).toBeVisible();
            const phoneEl = await component.findByText(phone);
            expect(phoneEl).toBeVisible();
            const companyEl = await component.findByText(companyName);
            expect(companyEl).toBeVisible();
            const callRateEl = await component.findByText(callRate);
            expect(callRateEl).toBeVisible();
            const dateEl = await component.findByText(lastContactedDateTime);
            expect(dateEl).toBeVisible();
        });
    });
    describe('Component should display a message when empty data is passed', () => {
        beforeEach(() => {
            component = render(<ServiceProviderTable data={[]} />);
        });
        afterEach(() => {
            component.unmount();
        });
        it ('should display no data message', async () => {
            const noDataMessageEl = await component.findByText('No records to display');
            expect(noDataMessageEl).toBeVisible();
        });
    });
});
