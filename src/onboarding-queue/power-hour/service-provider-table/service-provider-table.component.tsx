import React from 'react';
import MaterialTable from '@material-table/core';
import './styles.scss';
import { ServiceProviderTableProps } from './service-provider-table.interface';
import { Link } from "@mui/material";

const ServiceProviderTable: React.FC<ServiceProviderTableProps> = props => {
    return (
        <MaterialTable
            columns={[
                {
                    title: 'Sp ID',
                    field: 'spId',
                    id: 'spId',
                    filtering: false,
                    render: rowData => <Link href={rowData.crmSpSummaryUrl} target="_blank">{rowData.spId}</Link>,
                },
                { title: 'Name', field: 'name', id: 'name', filtering: false },
                { title: 'Company Name', field: 'companyName', id: 'companyName', filtering: false },
                { title: 'Phone #', field: 'phone', id: 'phone', filtering: false },
                { title: 'Rank', field: 'rank', id: 'rank', filtering: false, width: '10%' },
                { title: 'Status', field: 'enrollmentStatus', id: 'enrollmentStatus', filtering: false },
                { title: '# of ratings', field: 'numberOfRatings', type: 'numeric', id: 'numberOfRatings', filtering: false, width: '10%' },
                { title: 'Call Rate %', field: 'callRate', id: 'callRate', filtering: false, width: '10%' },
                {
                    title: 'Last Call Date Time',
                    field: 'lastContactedDateTime',
                    id: 'lastContactedDateTime',
                    type: 'datetime',
                    customSort: (a, b) => {
                        return +a.lastContactedDateTime - +b.lastContactedDateTime;
                    },
                    filtering: false
                }
            ]}
            data={props.data}
            title="Power Hour"
            options={{
                filtering: false,
                grouping: true,
                rowStyle: { textAlign: 'left' },
                filterRowStyle: { textAlign: 'left' },
                editCellStyle: { textAlign: 'left' },
                sorting: true,
                tableLayout: "fixed"
            }}
            isLoading={props.loading}
        />
    );
};

export default ServiceProviderTable;
