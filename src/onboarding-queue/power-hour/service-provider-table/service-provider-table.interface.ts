export interface EnrollmentStatus {
    enrollmentStatus: string;
    statusCode: number;
    subStatusCode: number;
}

export interface PowerHourServiceProvider {
    spId: number;
    lastContactedDateTime: string;
    name: string;
    companyName: string;
    phone: string;
    enrollmentStatus: string;
    numberOfRatings: number;
    callRate: number;
    rank: number;
    rankDescription: string;
    crmSpSummaryUrl: string;
}

export interface ServiceProviderTableProps {
    loading?: boolean;
    data: PowerHourServiceProvider[];
}
