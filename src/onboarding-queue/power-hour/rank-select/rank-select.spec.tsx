import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import RankSelect from './rank-select.component';
import { act } from 'react-dom/test-utils';

describe('Testing RankSelect', () => {
    let component: RenderResult;
    let rank = 1;
    const onChange = (val: number): void => {rank = val};
    beforeEach(() => {
        component = render(
            <RankSelect ranks={[{ rank: 1, rankReason: 'Test' }]} value={rank} onChange={onChange} />
        );
    });
    afterEach(() => {
        component.unmount();
    });

    it('should display a select', async () => {
        const select = await component.findAllByText('Rank');
        act(() => {
            fireEvent.click(select[0]);
        });
        expect(select[0]).toBeVisible();
    });
});
