import React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { SelectChangeEvent } from '@mui/material';
import { RankSelectProps } from './rank-select.interface';
import './styles.scss';

const RankSelect = (props: RankSelectProps) => {

  const handleChange = (event: SelectChangeEvent) => {
    if (props.value === parseInt(event.target.value)) return;
    props.onChange(parseInt(event.target.value));
  };

  return (
    <div className="rank-select--container">
        <Box sx={{ maxWidth: 120 }} >
          <FormControl fullWidth>
              <InputLabel id="rank-select-label">Rank</InputLabel>
              <Select
                  labelId="rank-select-label"
                  id="rank-select"
                  data-testid="rank-select"
                  value={props.value !== 1 ? props.value.toString() : ''}
                  defaultValue={props.value !== 1 ? props.value.toString() : ''}
                  label="Rank"
                  onChange={handleChange}
              >
                {props.ranks.length ? props.ranks.map(rank => (
                  <MenuItem value={rank.rank} key={rank.rank}>{rank.rank}</MenuItem>
                )): null}
              </Select>
          </FormControl>
        </Box>
    </div>
  );
};

export default RankSelect;
