import { Rank } from "../power-hour.interface";

export interface RankSelectProps {
    value: number;
    onChange: (val: number) => void;
    ranks: Rank[];
}
