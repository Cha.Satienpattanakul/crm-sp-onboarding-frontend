import { EnrollmentStatus } from "./service-provider-table/service-provider-table.interface";

export interface RawServiceProvidersList {
    companyName: string;
    lastContactedDateTime: string;
    name: string;
    queueId: number;
    rank: number;
    rankDescription: string;
    skipDateTime: string;
    spId: number;
    phoneNumber: string;
    timeZone: string;
    enrollmentStatus: EnrollmentStatus;
    numberOfRatings: number;
    callRate: number;
    crmSpSummaryUrl: string;
}

export interface Rank {
    rank: number;
    rankReason: string;
}

export interface RanksResponse {
    ranks: Rank[];
}
