import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import PowerHourContainer from './power-hour.component';
import { act } from 'react-dom/test-utils';

beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
});

describe('Testing PowerHourContainer', () => {
    let component: RenderResult;
    beforeEach(() => {
        component = render(
            <PowerHourContainer />
        );
    });
    afterEach(() => {
        component.unmount();
    });
    it('should display the table', async () => {
        const table = await component.findByText('Power Hour');
        expect(table).toBeInTheDocument();
    });
    it('should have a select', async () => {
        const select = await component.findByTestId('rank-select');
        act(async () => {
            await select.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        });
    });
    it('should display the table container correctly', async () => {
        const container = await component.findByTestId('pwc-container');
        expect(container).toBeInTheDocument();
    });
});
