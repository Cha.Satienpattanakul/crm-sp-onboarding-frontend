import request from "../shared/requests/request.api";
import { RanksResponse } from "./power-hour/power-hour.interface";

const baseUrl = '/api/sp-onboarding/priority-queue';

export const getServiceProviderRanks = async (): Promise<RanksResponse> => {
    try {
        const response = await request.get(`${baseUrl}/sp-ranks`);
        return response.data;
    } catch (error) {
        console.error(error);
        return { ranks: [] };
    }
};
