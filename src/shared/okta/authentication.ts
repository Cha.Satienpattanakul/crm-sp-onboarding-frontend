import request from "../../shared/requests/request.api";
const baseUrl = '/api/authentication/user/roles'

export async function getUserRoles(): Promise<[]> {
    return request.get(`${baseUrl}`)
        .then(response => {
            return  response.data;
        });
}