import React from 'react';
import { Redirect } from 'react-router-dom';
import OktaSignInWidget from './okta-sign-in-widget';
import { useOktaAuth } from '@okta/okta-react';

// @ts-ignore
const Login = ({ config }) => {
    const { oktaAuth, authState } = useOktaAuth();

    const onSuccess = (tokens: any) => {
        oktaAuth.handleLoginRedirect(tokens);
    };

    const onError = (err: any) => {
        console.log('error logging in', err);
    };

    if (!authState) return null;

    return (
        <>
            {authState.isAuthenticated
                ? <Redirect to={{ pathname: '/' }}/>
                : (
                    <OktaSignInWidget
                    config={config}
                    onSuccess={onSuccess}
                    onError={onError}/>
                )}
        </>
    )
};

export default Login;
