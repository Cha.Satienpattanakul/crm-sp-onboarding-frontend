const oktaAuthConfig = {
    // Note: If your app is configured to use the Implicit flow
    // instead of the Authorization Code with Proof of Code Key Exchange (PKCE)
    // you will need to add `pkce: false`
    issuer: 'https://angi.oktapreview.com/oauth2/default',
    clientId: '0oa1vckwl3qJ3dRzy1d7',
    redirectUri: window.location.origin + '/login/callback',
};

const oktaSignInConfig = {
    baseUrl: 'https://angi.oktapreview.com',
    clientId: '0oa1vckwl3qJ3dRzy1d7',
    redirectUri: window.location.origin + '/login/callback',
    logo: 'https://ok12static.oktacdn.com/fs/bcg/4/gfs1eqbj5pcQaZmbJ5d7',
    authParams: {
        // If your app is configured to use the Implicit flow
        // instead of the Authorization Code with Proof of Code Key Exchange (PKCE)
        // you will need to uncomment the below line
        // pkce: false
    }
    // Additional documentation on config options can be found at https://github.com/okta/okta-signin-widget#basic-config-options
};

export { oktaAuthConfig, oktaSignInConfig };
