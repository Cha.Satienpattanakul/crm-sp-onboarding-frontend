export interface AdminToolsTableBody {
    id: number,
    name: string;
    tableCells: string[];
}
