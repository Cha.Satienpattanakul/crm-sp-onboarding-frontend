export interface NextServiceProviderDetail {
    name: string;
    companyName: string;
    phoneNumber: string;
    rankDescription: string;
    rank: number;
    crmSpSummaryUrl: string;
    spId: number;
}

export interface CsmDetails {
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export interface NextServiceProviderResponse {
    nextSpDetails: NextServiceProviderDetail;
    csmDetails: CsmDetails;
}
