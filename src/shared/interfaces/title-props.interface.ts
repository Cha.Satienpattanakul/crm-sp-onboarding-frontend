export interface TitleProps {
    drawerWidth: number;
    drawerOpen: boolean;
    toggleDrawer: () => void;
}
