import {OverridableComponent} from "@mui/material/OverridableComponent";
import {SvgIconTypeMap} from "@mui/material";

export interface ActionIconButton {
    icon: OverridableComponent<SvgIconTypeMap>;
    label: string;
    path?: string;
    handleAction: any;
}

export interface DialogInput {
    name: string;
    id: number;
}
