export interface SideNavigationItems {
  title: string;
  elementId: string;
  path: string;
  hasRole?: string[];
  Icon?: any;
}
