import TitleBar from '../../dashboard/title-bar/title-bar.component';
import React from 'react';
import { LayoutProps } from './layout.interface';

const Layout: React.FC<LayoutProps> = (props) => {
  return (
    <>
      <TitleBar
        drawerOpen={props.drawerOpen}
        drawerWidth={props.drawerWidth}
        toggleDrawer={props.toggleDrawer}
        pageTitle={props.pageTitle}
        Icon={props.Icon}
      />
      <div className="basic-layout__content">{props.children}</div>
    </>
  );
};

export default Layout;
