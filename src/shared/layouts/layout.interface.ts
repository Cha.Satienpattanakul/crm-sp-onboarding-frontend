export interface LayoutProps {
  toggleDrawer: () => void;
  drawerWidth: number;
  drawerOpen: boolean;
  pageTitle: string;
  Icon?: any;
}
