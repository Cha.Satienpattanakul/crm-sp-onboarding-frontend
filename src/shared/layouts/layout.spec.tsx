import Layout from "./layout.component";
import React from 'react';
import { RenderResult, render } from "@testing-library/react";
import '@testing-library/jest-dom';

jest.mock('@okta/okta-react', () => ({
    useOktaAuth: () => {
        return {
            authState: {},
            authService: {},
            oktaAuth: {}
        };
    },
}));

describe('Testing BasicLayout', () => {
    const drawerWidth = 230;
    const pageTitle = 'Test Title';
    let drawerOpen = false;
    const toggleDrawer = (): void => {drawerOpen = !drawerOpen};
    let component: RenderResult;

    beforeEach(() => {
        component = render(
            <Layout
                drawerWidth={drawerWidth}
                drawerOpen={drawerOpen}
                toggleDrawer={toggleDrawer}
                pageTitle={pageTitle}
            >
                <h1>Test Content</h1>
            </Layout>
        );
    });
    afterEach(() => {
        component.unmount();
    });

    it('should display titleBar correctly', async () => {
        const title = await component.findByText(pageTitle);
        expect(title).toBeVisible();
    });

    it('should display the children correctly', async () => {
        const content = await component.findByText('Test Content');
        expect(content).toBeVisible();
    });
});
