import axios from "axios";
import {OktaAuth} from "@okta/okta-auth-js";
import {oktaAuthConfig} from "../okta/config";

const request = axios.create({
    baseURL: process.env.REACT_APP_CRM_URL
});

request.interceptors.request.use((request) => {
    const authClient = new OktaAuth(oktaAuthConfig);
    const accessToken = authClient.getAccessToken();
    request.headers.Authorization = `Bearer ${accessToken}`;
    return request;
}, (error) => {
    return Promise.reject(error);
});

export default request;
