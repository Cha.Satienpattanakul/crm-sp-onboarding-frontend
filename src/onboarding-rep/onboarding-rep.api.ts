import request from "../shared/requests/request.api";
import { OnboardingRepresentative } from "./onboarding-representative/onboarding-representative.interface";

const onboardingRepUrl = '/api/onboarding-rep';

export async function getOnboardingRep(): Promise<OnboardingRepresentative[]> {
    const response = await request.get(`${onboardingRepUrl}`);
    return response.data as OnboardingRepresentative[];
}