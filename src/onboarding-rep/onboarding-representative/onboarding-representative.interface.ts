export interface OnboardingRepresentative {
    id: number;
    name: string;
}

