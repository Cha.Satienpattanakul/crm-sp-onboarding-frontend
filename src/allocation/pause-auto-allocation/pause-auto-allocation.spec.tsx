import * as React from 'react';
import { RenderResult, render } from '@testing-library/react';
import PauseAutoAllocation from './pause-auto-allocation.component';
import '@testing-library/jest-dom';

describe('Testing PauseAutoAllocation', () => {
    let component: RenderResult;
    beforeEach(() => {
        component = render(
            <PauseAutoAllocation />
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it('Should display succesfull', async () => {
        const content = await component.findByText('Representatives in Paused Auto Allocation');
        expect(content).toBeVisible();
    });
});




