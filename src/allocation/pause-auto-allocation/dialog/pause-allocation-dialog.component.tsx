import * as React from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Stack} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import { PauseAllocationDialogProps } from '../pause-auto-allocation.interface';
import {makeStyles, createStyles} from "@mui/styles";
import {useEffect} from "react";
import {getOnboardingReps, getPauseAllocations} from "../../allocation.api";


const useStyles = makeStyles((theme) => createStyles({
    root: {
        minWidth: 0,
        margin: 'auto',
        alignItems: 'center',
        justifyContent: 'center'
    },
}))

const PauseAllocationDialog = ({ onClose, open, onSave }: PauseAllocationDialogProps) => {
    const classes = useStyles();
    const handleClose = () => {
        onClose();
    };
    

    const [representativeId, setRepresentativeId] = React.useState<number>(-1);
    const [onboardingReps, setOnboardingReps] = React.useState<any>([]);

    const handleSave = () => {
        let selectedRep = onboardingReps.filter((p: any ) => p.id === representativeId)[0]
        selectedRep = {...selectedRep, onboardingRepId: selectedRep.id}
        onSave(selectedRep);
    }
    const handleChange = (event: any, value: any) =>{
        setRepresentativeId(value.id);
    }

    const loadOnboardingRepresentatives = async () => {
        const allocations = await getPauseAllocations()
        let reps = await getOnboardingReps()
        let repArray: string[] = []
        allocations.map(rep => {
            repArray.push(rep.onboardingRepName)
        })
        reps = reps.filter(rep => !repArray.includes(rep.name))
        reps.sort((a, b) => a.name.localeCompare(b.name))
        setOnboardingReps(reps)
    }

    useEffect(() => {
        loadOnboardingRepresentatives()
        return () => {
            setOnboardingReps([])
        }
    },[])

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Pause Auto Allocation For Rep</DialogTitle>
            <DialogContent>
                <FormControl variant="standard" sx={{ m: 2, minWidth: 120 }}>
                    <Autocomplete
                        disablePortal
                        id="combo-box-demo"
                        onChange={handleChange}
                        options={onboardingReps}
                        onOpen={ async () => await loadOnboardingRepresentatives()}
                        getOptionLabel={({name}) => name}
                        fullWidth={true}
                        sx={{ width: 300, height: 400}}
                        renderInput={(params) => <TextField {...params} label="Select" />}
                    />
                </FormControl>
            </DialogContent>
            <DialogActions className={classes.root}>
                <Stack spacing={3} direction='row'>
                <Button variant="outlined" color='error' sx={{width: 88}} onClick={handleClose}>Cancel</Button>
                <Button disableElevation={true} sx={{width: 88}} variant="contained" color='primary' onClick={handleSave}>Save </Button>
                </Stack>
            </DialogActions>
        </Dialog>
    );
}

export default PauseAllocationDialog;
