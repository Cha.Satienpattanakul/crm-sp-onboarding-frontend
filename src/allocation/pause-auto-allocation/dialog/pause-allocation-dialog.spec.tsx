import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import PauseAllocationDialog from './pause-allocation-dialog.component';
import { PauseAllocationDialogProps, PauseAutoAlloc } from '../pause-auto-allocation.interface';

describe('Testing PauseAutoAllocation', () => {
    let component: RenderResult;
    const props: PauseAllocationDialogProps = {
        open: true,
        reps: [{}],
        onClose: () => {},
        onSave: (value: PauseAutoAlloc) => {}
    }
    beforeEach(() => {
            component = render(
            <PauseAllocationDialog {...props}/>
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it('Should display succesfull', async () => {
        const content = await component.findByText('Pause Auto Allocation For Rep');
        expect(content).toBeVisible();
    });
});




