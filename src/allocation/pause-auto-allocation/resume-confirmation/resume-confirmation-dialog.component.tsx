import * as React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { ResumeConfirmationDialogProps } from '../pause-auto-allocation.interface';


const ResumeConfirmationDialog = ({ onClose, onResume, open, rep }: ResumeConfirmationDialogProps) => {
    
    const handleClose = () => {
        onClose();
    }

    const handleResume = () => {
        onResume(rep);
    }

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Are you sure?</DialogTitle>
            <DialogContent data-testid="content">
                Resume Allocation to <strong>{rep.label}</strong>?
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleResume}>Resume</Button>
            </DialogActions>
        </Dialog>
    );
}

export default ResumeConfirmationDialog;