import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import ResumeConfirmationDialog from './resume-confirmation-dialog.component';
import { PauseAutoAlloc } from '../pause-auto-allocation.interface';



describe('Testing ResumeConfirmationDialog', () => {
    let component: RenderResult;
    const onResumeDialogClose = () => { };
    const onResume = (rep: PauseAutoAlloc) => { };
    const openResumeDialog = true;
    const selectedRep: PauseAutoAlloc = {
        id: 1,
        onboardingRepId: 12,
        label: 'testUser'
    }
    beforeEach(() => {
        component = render(
            <ResumeConfirmationDialog 
            onClose={onResumeDialogClose} 
            onResume={onResume} 
            open={openResumeDialog} 
            rep={selectedRep}/>
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it('Should display successfully', async () => {
        const content = await component.findAllByTestId('content')
        expect(content[0]).toBeVisible();
    });
});