import * as React from 'react';
import { useEffect, useState } from 'react';
import TableToolbar from './table/table-toolbar.component';
import MaterialTable from '@material-table/core';
import ResumeConfirmationDialog from './resume-confirmation/resume-confirmation-dialog.component';
import { getPauseAllocations, saveResumeAllocation } from '../allocation.api';
import { PauseAutoAlloc, TableRowData, ToolbarData } from './pause-auto-allocation.interface';
import {Box, Paper, Alert} from "@mui/material";

import './pause-allocation.css';

const PauseAutoAllocation = () => {
    const [rows, setRows] = React.useState<TableRowData[]>([])
    const [openResumeDialog, setOpenResumeDialog] = React.useState<boolean>(false);
    const [selectedRepresentative, setSelectedRepresentative] = React.useState<PauseAutoAlloc>({} as PauseAutoAlloc);
    const [pauseAllocations, setPauseAllocations] = useState<ToolbarData[]>([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState<boolean>(false)

    const actionColumnStyle = () => {
        const table = document.getElementsByTagName('table')[0];
        const tbody = table.getElementsByTagName('tbody')[0];
        const tr:  HTMLCollectionOf<HTMLTableRowElement> = tbody.getElementsByTagName('tr');
        for(let i = 0; i < tr.length; i++) {
            const td = tr[i].getElementsByTagName('td')[1];
            if(td !== undefined && td.hasChildNodes()) {
                const div = td.getElementsByTagName('div')[0];
                div.style.alignItems = 'center';
                div.style.justifyContent = 'center';
            }
        }
    }
    const sortPausedAllocations = (a: PauseAutoAlloc, b: PauseAutoAlloc) => {
        if (a.label < b.label) {
            return -1;
        }
        if (a.label > b.label) {
            return 1;
        }
        return 0;
    }

    const loadPauseAllocations = async () => {
        setLoading(true)
        try {
            const resp = await getPauseAllocations()
            const x = resp.map(r => {
                return {id: r.id, label: r.onboardingRepName, onboardingRepId: r.onboardingRepId}
            }).sort(sortPausedAllocations)
            if (x && x.length > 0) {
                setPauseAllocations(x);
                let rows: TableRowData[] = []
                x.forEach(value => {
                    const row = {name: value.label, action: '', id: value.id, repId: value.onboardingRepId}
                    rows.push(row)
                })
                setRows(rows)
            }
        }catch (err){
            setError(true)
        }
        setLoading(false)
    }

    useEffect(() => {
        loadPauseAllocations()
        return () => {
            setPauseAllocations([])
            setError(false)
        }
    }, [])
    const selectedData = async (row: TableRowData) => {
        await loadPauseAllocations()
        setPauseAllocations(pauseAllocations.filter(p => p.id !== row.id).sort(sortPausedAllocations));
        actionColumnStyle();
    }
    const handleResumeClick = (row: any) => {
        setSelectedRepresentative({ id: row.id, label: row.name, onboardingRepId: row.repId });
        setOpenResumeDialog(true);
    }

    const onResume = (rep: PauseAutoAlloc) => {
        setLoading(true)
        setOpenResumeDialog(false);
        saveResumeAllocation(rep.id).then(resp => {
            setRows(rows.filter(r => r.id !== rep.id))
        }).catch(err => {
            setError(true)
        });
        setTimeout(()=>{
            setLoading(false)
        }, 1000)
    }
    const onResumeDialogClose = () => {
        setOpenResumeDialog(false);
    }
    return (
        <Box sx={{ width: '100%' }}>
            {
                error && (
                    <Alert sx={{mb:4}} severity="error" onClose={() => {setError(false)}}>
                        Something went wrong. Please try again or contact the development team.
                    </Alert>
                )
            }
            <Paper sx={{ width: '100%', mb: 2 }}>
                <TableToolbar selectedPauseAllocation={selectedData} reps={pauseAllocations} />
                <ResumeConfirmationDialog onClose={onResumeDialogClose} onResume={onResume} open={openResumeDialog} rep={selectedRepresentative} />
                
                <MaterialTable
                    columns={[{ title: 'Name', field: 'name', id: 'name', filtering: true, width: '50%' },
                    ]}
                    data={rows}
                    title=""
                    actions={[
                        {
                          icon: 'play_arrow',
                          tooltip: 'Resume',
                          onClick: (event, rowData) => {
                            handleResumeClick(rowData);
                          }
                        }
                      ]}
                    options={{
                        sorting: true,
                        rowStyle: { textAlign: 'center', marginLeft: 50 },
                        filterRowStyle: { textAlign: 'center' , marginLeft: 50 },
                        editCellStyle: { textAlign: 'center' },
                        actionsCellStyle: {position: 'relative', justifyContent: 'center'},
                        actionsColumnIndex: -1,
                        pageSize: 10,
                        pageSizeOptions: [10, 20, 30],
                        tableLayout: 'fixed',
                    }}
                    isLoading={loading}
                />

            </Paper>

        </Box>
    );
}
export default PauseAutoAllocation;