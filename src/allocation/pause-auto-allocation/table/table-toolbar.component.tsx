import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';

import {Alert, Button, Grid, Typography} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import PauseAllocationDialog  from '../dialog/pause-allocation-dialog.component';
import { PauseAutoAlloc, TableRowData, ToolbarData } from '../pause-auto-allocation.interface';
import { savePauseAllocation } from '../../allocation.api';

interface TableToolbarProps {
    selectedPauseAllocation: (value: TableRowData) => void;
    reps: ToolbarData[];
}

const TableToolbar = ({ selectedPauseAllocation, reps }: TableToolbarProps) => {
    const [open, setOpen] = React.useState(false);
    const [error, setError] = React.useState(false);


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleSave = (val: PauseAutoAlloc) => {
        setOpen(false);
        
        savePauseAllocation(val.onboardingRepId).then(resp => {
            selectedPauseAllocation({ name: val.label, action: "", repId: val.onboardingRepId, id: val.id });
        }).catch(err => {
            setError(true)
        });
        
    }
    return (
        <>
            <Toolbar
                sx={{
                    pl: { sm: 2 },
                    pr: { xs: 1, sm: 1 },
                }}
            >
                {
                    error && (
                        <Alert sx={{mb:4}} severity="error" onClose={() => {setError(false)}}>
                            Something went wrong. Please try again or contact the development team.
                        </Alert>
                    )
                }
                <Grid container spacing={2}>
                    <Grid item xs={10}>
                    <Typography variant="h5" sx={{fontWeight: 'bold'}}>Representatives in Paused Auto Allocation</Typography>
                    </Grid>
                    <Grid item xs={2}>
                        <Button variant="outlined" onClick={handleClickOpen} startIcon={<AddIcon/>}>Add Rep</Button>
                    </Grid>
                </Grid>

            </Toolbar>
            <PauseAllocationDialog
                open={open}
                onClose={handleClose}
                onSave={handleSave}
                reps={reps}
            />
        </>
    );
};

export default TableToolbar;