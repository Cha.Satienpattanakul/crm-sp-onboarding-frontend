import { RenderResult, render } from '@testing-library/react';
import React from 'react';
import TableToolbar from './table-toolbar.component';
import '@testing-library/jest-dom';
import { TableRowData, ToolbarData } from '../pause-auto-allocation.interface';


describe('Testing TableToolbar', () => {
    let component: RenderResult;
    const selectedData = (value: TableRowData) => {};
    const reps: ToolbarData[] = [{
        id: 1,
        onboardingRepId: 12,
        label: 'testUser'
    }]

    beforeEach(() => {
        component = render(
            <TableToolbar selectedPauseAllocation={selectedData} reps={reps} />
        )
    });
    afterEach(() => {
        component.unmount();
    });
    it('Should display succesfull', async () => {
        const content = await component.findByText('Add Rep');
        expect(content).toBeVisible();
    });
});