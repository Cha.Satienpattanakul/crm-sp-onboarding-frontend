  export interface PauseAllocationDialogProps {
    open: boolean;
    reps: any[];
    onClose: () => void;
    onSave: (value: PauseAutoAlloc) => void;
  }

  export interface PauseAutoAllocResp {
    id: number;
    onboardingRepId: number;
    onboardingRepName: string;
  }

  export interface PauseAutoAlloc {
    id: number;
    onboardingRepId: number;
    label: string;
  }

  export interface TableRowData {
    name: string;
    action: string;
    id: number;
    repId: number;
  }

  export interface ResumeConfirmationDialogProps {
    open: boolean;
    rep: PauseAutoAlloc;
    onClose: () => void;
    onResume: (seletedRep: PauseAutoAlloc) => void;

  }

  export interface ToolbarData {
    id: number;
    label: string;
    onboardingRepId: number;
  }

  export interface OnboardingRep {
    id: number;
    name: string;
  }