import request from "../shared/requests/request.api";
import {OnboardingRep, PauseAutoAllocResp} from "./pause-auto-allocation/pause-auto-allocation.interface";

const baseUrl = '/api/allocation'
const repUrl = '/api/onboarding-rep';

export async function savePauseAllocation(id: number): Promise<void> {
    const response = await request.post(`${baseUrl}/pause`, {"onboardingRepId": id});
    return response.data;
}
export async function saveResumeAllocation(id: number): Promise<void> {
    const response = await request.post(`${baseUrl}/resume`, {"spAutoAllocationPauseId": id});
    return response.data;
}

export async function getPauseAllocations(): Promise<PauseAutoAllocResp[]> {
    const response = await request.get(`${baseUrl}/pause`);
    return response.data;
}

export async function getOnboardingReps(): Promise<OnboardingRep[]> {
    const response = await request.get<OnboardingRep[]>(`${repUrl}`);
    return response.data;
}
