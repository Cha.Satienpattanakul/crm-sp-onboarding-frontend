import React, {FC} from "react";
import {
    Link,
    Table, TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@mui/material";
import { Link as RouterLink } from 'react-router-dom';
import {styled} from "@mui/material/styles";
import ActionColumn from "./action-column.component";
import { ActionIconButton, AdminToolsTableBody } from "@src/shared/interfaces";

interface IAdminToolsTable {
    label: string;
    labelBy: string;
    headers: string[];
    body: AdminToolsTableBody[];
    actionIconButtons: ActionIconButton[];
    hasActionColumn: boolean;
    rowsPerPageOptions: number[];
    rowsPerPage: number;
    page: number;
    path: string;
    totalCount: number;
    handlePageChange: (event:any, newPage: number) => void;
    handleRowsPerPageChange: (event:any) => void;
}

const AdminToolTable: FC<IAdminToolsTable> = (
    {
        label,
        labelBy,
        headers,
        body,
        actionIconButtons,
        hasActionColumn,
        rowsPerPageOptions,
        rowsPerPage,
        page,
        path,
        totalCount,
        handlePageChange,
        handleRowsPerPageChange
    }) => {

    const classes = {
        spaceTop: `spaceTop`
    }
    const Root = styled('div')(({theme}) => ({

        [`&.${classes.spaceTop}`]: {
            marginTop: '2em'
        }
    }));

    return(
        <Root>
            <TableContainer className={classes.spaceTop}>
                <Table aria-label={label} size={'small'} aria-labelledby={labelBy}>
                    <TableHead>
                        <TableRow>
                            {headers.map((header, index) => (
                                <TableCell key={index} align="left">{header}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {body.map((row) => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    <Link component={RouterLink} to={`${path}/${row.id}`}>
                                        {row.name}
                                    </Link>
                                </TableCell>
                                {row.tableCells.map((cell, index) => (
                                    <TableCell key={index} align="left">{cell}</TableCell>
                                ))}
                                {hasActionColumn && (
                                    <ActionColumn
                                        actionIconButtons={actionIconButtons}
                                        rowId={row.id}
                                        rowName={row.name}
                                    />
                                )}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={rowsPerPageOptions}
                component="div"
                count={totalCount}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}
            />
        </Root>
    )
};

export default AdminToolTable;
