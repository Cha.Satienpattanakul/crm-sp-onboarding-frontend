import React, {FC} from "react";
import { IconButton, TableCell } from "@mui/material";
import { ActionIconButton } from "../shared/interfaces";

interface IActionColumn {
    rowName: string;
    rowId: number;
    actionIconButtons: ActionIconButton[];
}

const ActionColumn: FC<IActionColumn> = ({rowName, actionIconButtons, rowId}) => {
    return (
        <TableCell align="left">
            {actionIconButtons.map((actionIcon, index) => {
                let Icon = actionIcon.icon;
                const args = actionIcon.path ? `${actionIcon.path}/${rowId}` : {name: rowName, id: rowId};
                return (
                    <IconButton
                        aria-label={`${actionIcon.label} ${rowName}`}
                        title={`${actionIcon.label} ${rowName}`}
                        onClick={() => actionIcon.handleAction(args)}
                        key={index}
                    >
                        <Icon/>
                    </IconButton>
                )
            })}
        </TableCell>
    )
};

export default ActionColumn;
