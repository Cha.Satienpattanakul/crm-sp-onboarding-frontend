import React, {FC} from "react";
import {Button, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";

interface ITableHeaderBar {
    title: string;
    titleElementId: string;
    hasButton: boolean;
    buttonTitle?: string;
    buttonElementId?: string;
    handlePageRedirect?: () => void;
}

const TableHeaderBar: FC<ITableHeaderBar> = (
    {
        title,
        titleElementId,
        hasButton,
        buttonTitle,
        buttonElementId,
        handlePageRedirect
    }) => {

    const classes = {
        root: `root`,
        title: `title`
    }
    const Root = styled('div')(({theme}) => ({

        [`&.${classes.root}`]: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(1)
        },
        [`&.${classes.title}`]: {
            flex: '1 1 1%'
        }
    }));

    return(
        <Root>
        <Toolbar className={classes.root}>
            <Typography
                variant="h6"
                id={titleElementId}
                component="div"
                className={classes.title}
            >
                {title}
            </Typography>
            {hasButton ? (
                <Button
                    variant={'contained'}
                    color={'secondary'}
                    id={buttonElementId}
                    onClick={handlePageRedirect}
                >
                    {buttonTitle}
                </Button>
            ) : ''}
        </Toolbar>
    </Root>
    )
};

export default TableHeaderBar;
