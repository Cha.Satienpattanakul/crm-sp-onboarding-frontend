import React, { FC } from 'react';
import { Button, IconButton, Toolbar, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { useOktaAuth } from '@okta/okta-react';
import { styled } from '@mui/material/styles';
import { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar/AppBar';
import MuiAppBar from '@mui/material/AppBar';
import './title-bar.scss';
import NorthStarLogo from '../../assets/img/NorthStarLogo.png';
import { TitleProps } from '../../shared/interfaces';

interface ITitleBar extends TitleProps {
  pageTitle: string;
  Icon?: any;
}

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const TitleBar: FC<ITitleBar> = React.memo(
  ({ drawerWidth, drawerOpen, toggleDrawer, pageTitle, Icon }) => {
    const { oktaAuth } = useOktaAuth();
    const AppBar = styled(MuiAppBar, {
      shouldForwardProp: (prop) => prop !== 'open',
    })<AppBarProps>(({ theme, open }) => ({
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
      }),
    }));

    const logout = async () => oktaAuth.signOut();

    const goToBetti = (): void => {
      const link = process.env.REACT_APP_BETTI_LINK;
      window.open(link);
    };

    return (
      <AppBar elevation={0} position="fixed" open={drawerOpen}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            edge="start"
            sx={{ mr: 0.5 }}
          >
            {drawerOpen ? <Icon /> : <MenuIcon />}
          </IconButton>
          <Typography
            sx={{ fontWeight: 'bold' }}
            variant="h4"
            noWrap
            className={pageTitle}
          >
            {pageTitle}
          </Typography>
          <div className="buttons--wrapper">
            <Button color="inherit" onClick={logout}>
              Logout
            </Button>
            <Button color="inherit" onClick={goToBetti}>
              BETTI
            </Button>
          </div>
          <img
            src={NorthStarLogo}
            alt="North Star Logo"
            className={'north-star-logo'}
          />
        </Toolbar>
      </AppBar>
    );
  }
);

TitleBar.displayName = 'TitleBar';

export default TitleBar;
