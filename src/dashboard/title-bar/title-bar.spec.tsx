import TitleBar from './title-bar.component';
import React from 'react';
import { act, RenderResult, render } from '@testing-library/react';
import IconDashboard from '@mui/icons-material/Dashboard';

jest.mock('@okta/okta-react', () => ({
  useOktaAuth: () => {
    return {
      authState: {},
      authService: {},
    };
  },
}));

describe('TitleBar component', () => {
  const drawerWidth = 230;
  const pageTitle = 'Test Title';
  let drawerOpen = false;
  const toggleDrawer = (): void => {
    drawerOpen = !drawerOpen;
  };
  const Icon = IconDashboard;

  let component: RenderResult;

  beforeEach(() => {
    component = render(
      <TitleBar
        drawerWidth={drawerWidth}
        pageTitle={pageTitle}
        toggleDrawer={toggleDrawer}
        drawerOpen={drawerOpen}
        Icon={Icon}
      />
    );
  });

  afterEach(() => {
    component.unmount();
  });

  it('should display the TitleBar component and show a title', () => {
    const title = document.getElementsByClassName(pageTitle)[0];
    expect(title.textContent).toBe(pageTitle);
  });

  it('should display the TitleBar component and trigger the toggleDrawer function', () => {
    const drawerButton = document.querySelector('button');
    act(() => {
      drawerButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });
    expect(drawerOpen).toBe(true);
  });
});
