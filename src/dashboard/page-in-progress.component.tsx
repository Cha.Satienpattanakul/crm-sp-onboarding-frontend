import React, { FC } from 'react';
import { TitleProps } from '../shared/interfaces';
import TitleBar from './title-bar/title-bar.component';

const PageInProgress: FC<TitleProps> = ({
  drawerWidth,
  drawerOpen,
  toggleDrawer,
}) => {
  return (
    <TitleBar
      drawerWidth={drawerWidth}
      drawerOpen={drawerOpen}
      toggleDrawer={toggleDrawer}
      pageTitle={'Page is in Progress'}
    />
  );
};

export default PageInProgress;
