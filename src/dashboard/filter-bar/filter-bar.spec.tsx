import FilterBar from "./filter-bar.component";
import { act } from "react-dom/test-utils";
import React from 'react';
import { RenderResult, render } from "@testing-library/react";

describe('FilterBar component', () => {
    const ariaLabel = 'test-aria-label';
    const menuId = 'test-menu-id';
    const title = 'Test Title';
    const options = [{display: 'option1', value: 1}]
    let type = '';
    let value: any = null;
    const setFilter = (filterType: string, filterValue: any) => {
        type = filterType;
        value = filterValue;
    };
    let component: RenderResult;

    beforeEach(() => {
        component = render(
            <FilterBar
                ariaLabel={ariaLabel}
                menuId={menuId}
                title={title}
                options={options}
                setFilter={setFilter}
            />
        );
    });

    afterEach(() => {
        component.unmount();
    });

    it('should display a FilterBar component and show its title', async () => {        
        const button = document.querySelector('button');
        expect(button?.textContent).toBe(title);
    });
    it('should display FilterBar and handle menu click and have a value', async () => {
        const menu = document.getElementById(`${menuId}-${options[0].display}`);
        await act(async () => {
            await menu?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        });
        expect(type).toBe(title);
        expect(value).toBe(options[0].value);
    });
})