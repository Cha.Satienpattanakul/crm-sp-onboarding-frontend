import React, {FC} from "react";
import {Button, Grid, Menu, MenuItem} from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { FilterOptions } from "../../shared/interfaces";


interface IFilterBar {
    ariaLabel: string;
    menuId: string;
    title: string;
    options: FilterOptions[];
    setFilter: (filterType: string, filterValue: any) => void;
}

const FilterBar: FC<IFilterBar> = ({ariaLabel, menuId, title, options, setFilter}) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedFilter, setSelectedFilter] = React.useState('');

    const handleToggle = (event: any): void => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (): void => {
        setAnchorEl(null);
    };

    const handleSelectFilter = (value: string): void => {
        setAnchorEl(null);
        setSelectedFilter(value);
        setFilter(title, value);
    };

    return(
        <Grid item>
            <Button
                variant={'outlined'}
                aria-label={ariaLabel}
                aria-haspopup="true"
                aria-controls={menuId}
                onClick={handleToggle}
            >
                {title}
                <ArrowDropDownIcon />
            </Button>
            <Menu
                id={menuId}
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {options.map((option, index) => (
                    <MenuItem
                        selected={option.value === selectedFilter}
                        onClick={() => handleSelectFilter(option.value)}
                        key={index}
                        data-testid={`${menuId}-${option.display}`}
                        id={`${menuId}-${option.display}`}
                    >
                        {option.display}
                    </MenuItem>
                ))}
            </Menu>
        </Grid>
    )
};

export default FilterBar;
