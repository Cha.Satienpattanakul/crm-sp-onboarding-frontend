import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { TitleProps } from '../shared/interfaces';

const Dashboard: FC<TitleProps> = ({drawerOpen, drawerWidth, toggleDrawer}) => {
  return (
    <>
      <div className="container flex h-screen max-w-full">
        <div className="m-auto text-black text-3xl">
          <h1>Welcome to the Onboarding Experience app.</h1>
          <p className="italic grid place-items-center pt-5">
            Dashboard Coming Soon....
          </p>
          <div className="grid place-items-center pt-5">
            <Link
              to="call-next-sp"
              className="items-center justify-center px-4 py-2 border border-transparent text-base font-medium rounded-md text-white bg-sky-600 hover:bg-sky-700 md:py-2 md:text-lg md:px-5"
            >
              Call Next SP
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
