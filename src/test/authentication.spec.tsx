import {getUserRoles} from "../shared/okta/authentication";
import request from "../shared/requests/request.api";
import {mockRejected, mockResolved} from "./utils";
import {ERROR} from "./utils/constants";
import { Role } from "../shared/role.enum";

it("mock getUserRoles success", () => {
    expect.assertions(1);
    request.get = mockResolved([Role.BccAdmin]);
    return getUserRoles().then(data => expect(data).toEqual([Role.BccAdmin]));
});

it('mock getUserRoles error', () => {
    expect.assertions(1);
    request.get = mockRejected(ERROR)
    return expect(getUserRoles()).rejects.toMatch(ERROR);
});