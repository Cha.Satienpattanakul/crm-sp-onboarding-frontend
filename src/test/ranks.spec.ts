import { getServiceProviderRanks } from '../onboarding-queue/ranks.api';
import request from '../shared/requests/request.api';
import { getRanksResponse } from './__mocks__/mock-data';
import { mockResolved } from './utils';

it('getRanks should return an array with ranks', async () => {
    expect.assertions(2);
    request.get = mockResolved(getRanksResponse);
    const data = await getServiceProviderRanks();
    expect(data.ranks[0].rank).toBe(1);
    expect(data.ranks[0].rankReason).toBe('test');
});

it('getRanks should get an error and return an empty array', async () => {
    expect.assertions(1);
    request.get = mockResolved({ ranks: [] });
    const data = await getServiceProviderRanks();
    expect(data.ranks[0]).toBe(undefined);
});
