import { PauseAutoAllocResp } from "../../allocation/pause-auto-allocation/pause-auto-allocation.interface";
import { Rank, RawServiceProvidersList } from "../../onboarding-queue/power-hour/power-hour.interface";
import { CsmDetails, NextServiceProviderDetail, NextServiceProviderResponse } from "../../shared/interfaces";

export const nextSp: NextServiceProviderDetail = {
    name: "Joseph Skalicky",
    companyName: "Test",
    phoneNumber: "0123456789",
    rankDescription: "Rank 1",
    rank: 1,
    crmSpSummaryUrl: "https://test.com",
    spId: 12345
}

const csmDetails: CsmDetails = {
    firstName: "Sam",
    lastName: "Harris",
    phoneNumber: "987654321"
}

export const nextSpResponse: NextServiceProviderResponse = {
    nextSpDetails : nextSp,
    csmDetails : csmDetails
}

export const pauseAutoAllocResp: PauseAutoAllocResp = {
    id: 1,
    onboardingRepId: 0,
    onboardingRepName: "Test"
}

export const powerHourResponse: RawServiceProvidersList[] = [
    {
        callRate: 0.1111111111111111,
        companyName: "Perfect Image Landscaping",
        enrollmentStatus: {
            enrollmentStatus: "Approved - Member",
            statusCode: 3,
            subStatusCode: 1
        },
        lastContactedDateTime: '11-23-11-T23:22:32',
        name: 'Test',
        queueId: 1,
        rank: 1,
        rankDescription: 'Test',
        skipDateTime: '11-23-11-T23:22:32',
        spId: 1,
        phoneNumber: '5555555555',
        timeZone: 'CST',
        numberOfRatings: 1,
        crmSpSummaryUrl: '#'
    }
];

export const getRanksResponse: { ranks: Rank[]} = {
    ranks: [
        {
            rank: 1,
            rankReason: 'test'
        }
    ]
};

export const skipServiceProviderResponse = {
    status: 200,
    statusText: ''
}