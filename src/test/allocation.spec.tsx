import {savePauseAllocation, saveResumeAllocation, getPauseAllocations} from "../allocation/allocation.api";
import request from "../shared/requests/request.api";
import {pauseAutoAllocResp} from "./__mocks__/mock-data";
import {mockRejected, mockResolved} from "./utils";
import {ERROR} from "./utils/constants";

it("mock savePauseAllocation success", () => {
    expect.assertions(1);
    request.post = mockResolved([pauseAutoAllocResp]);
    return savePauseAllocation(1).then(data => expect(data).toEqual([pauseAutoAllocResp]));
});

it('mock savePauseAllocation error', () => {
    expect.assertions(1);
    request.post = mockRejected(ERROR)
    return expect(savePauseAllocation(1)).rejects.toMatch(ERROR);
});

it("mock saveResumeAllocation success", () => {
    expect.assertions(1);
    request.post = mockResolved([pauseAutoAllocResp]);
    return saveResumeAllocation(1).then(data => expect(data).toEqual([pauseAutoAllocResp]));
});

it('mock saveResumeAllocation error', () => {
    expect.assertions(1);
    request.post = mockRejected(ERROR)
    return expect(saveResumeAllocation(1)).rejects.toMatch(ERROR);
});

it("mock getPauseAllocations success", () => {
    expect.assertions(1);
    request.get = mockResolved([pauseAutoAllocResp]);
    return getPauseAllocations().then(data => expect(data[0].onboardingRepName).toEqual("Test"));
});

it('mock getPauseAllocations error', () => {
    expect.assertions(1);
    request.get = mockRejected(ERROR)
    return expect(getPauseAllocations()).rejects.toMatch(ERROR);
});