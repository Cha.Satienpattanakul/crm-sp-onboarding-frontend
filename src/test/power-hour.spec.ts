import { powerHour } from '../onboarding-queue/power-hour/power-hour.api';
import request from '../shared/requests/request.api';

import { powerHourResponse } from './__mocks__/mock-data';
import { mockResolved } from './utils';
const rank = 1;

it('powerHour returns a success response with data', async () => {
    expect.assertions(1);
    request.get = mockResolved(powerHourResponse);
    const data = await powerHour();
    return expect(data[0].name).toBe('Test');
});

it('powerHour gets an error and returns an empty array', async () => {
    expect.assertions(1);
    request.get = mockResolved([]);
    const data = await powerHour();
    return expect(data[0]).toBe(undefined);
});
