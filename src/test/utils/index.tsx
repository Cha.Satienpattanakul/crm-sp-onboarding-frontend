
export const mockResolved = (data: {}) => {
    return jest.fn().mockResolvedValue({data : data});
};

export const mockRejected = (error: {}) => {
    return jest.fn().mockRejectedValue(error);
};