import request from "../shared/requests/request.api";
import {nextSpResponse} from "./__mocks__/mock-data";
import {mockRejected, mockResolved} from "./utils";
import {ERROR} from "./utils/constants";
import { getNextServiceProvider } from "../onboarding-queue/assignments/assignment.interface";
import { skipServiceProvider } from '../onboarding-queue/call-next-service-provider/next-service-provider-skip-dialog/next-service-provider-skip-dialog.api';

it("mock getNextSp success", () => {
    expect.assertions(1);
    request.get = mockResolved(nextSpResponse);
    return getNextServiceProvider().then(data => expect(data.nextSpDetails.name).toEqual("Joseph Skalicky"));
});

it('mock getNextSp error', () => {
    expect.assertions(1);
    request.get = mockRejected(ERROR)
    return expect(getNextServiceProvider()).rejects.toMatch(ERROR);
});

it('mock skipServiceProvider API call', async () => {
    expect.assertions(1);
    const data = await skipServiceProvider(1);
    return expect(data).toBe(false);
});
