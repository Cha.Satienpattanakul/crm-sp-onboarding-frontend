import React from 'react';
import '@testing-library/jest-dom'
import { act, RenderResult, render } from '@testing-library/react';
import * as ReactDOM from 'react-dom';
import App from '../../App';
import { MemoryRouter } from 'react-router';


jest.mock("../../shared/okta/login", () => {
  return <div>SignInWidgetMock</div>;
});

beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

describe('renders learn react link', () => {
  let component: RenderResult;

  beforeEach(() => {
    component = render(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
  });
  afterEach(() => {
    component.unmount();
  });
  it('should display the content', async () => {
    const content = document.querySelector('div');
    expect(content).toBeInTheDocument();
  });
});
