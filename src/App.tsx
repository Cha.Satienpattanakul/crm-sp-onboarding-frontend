import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { CssBaseline, styled, Box } from '@mui/material';
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';
import { SecureRoute, Security } from '@okta/okta-react';
import Login from './shared/okta/login';
import BasicLayout from './shared/layouts/layout.component';
import { Routes } from './navigation/routes';
import SideNav from './navigation/side-nav.component';
import PageInProgress from './dashboard/page-in-progress.component';
import { oktaAuthConfig, oktaSignInConfig } from './shared/okta/config';
import { getUserRoles } from './shared/okta/authentication';

const drawerWidth = 270;
const Main = styled('main', {
  shouldForwardProp: (prop) => prop !== 'drawerOpen',
})<{
  drawerOpen?: boolean;
}>(({ theme, drawerOpen }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(drawerOpen && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

function App() {
  const oktaAuth = new OktaAuth(oktaAuthConfig);
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [selectedLink, setSelectedLink] = React.useState(false);
  const [userRoles, setUserRoles] = useState([]);
  const history = useHistory();
  const drawerWidth = 270;

  const toggleDrawer = (): void => {
    setDrawerOpen(!drawerOpen);
  };

  const handleListItemClick = (path: string, title: string): void => {
    setSelectedLink(true);
    history.push(path);
  };

  const routes = Routes({
    drawerWidth: drawerWidth,
    drawerOpen: drawerOpen,
    toggleDrawer: toggleDrawer,
  });

  const sideNavItems = routes;

  const customAuthHandler = () => {
    history.push('/login');
  };

  const restoreOriginalUri = async (
    _oktaAuth: OktaAuth,
    originalUri: string
  ) => {
    await loadUserRoles();
    history.replace(toRelativeUrl(originalUri, window.location.origin));
  };
  const loadUserRoles = async () => {
    try {
      const result = await getUserRoles();
      setUserRoles(result);
    } catch (error) {
      setUserRoles([]);
    }
  };
  useEffect(() => {
    oktaAuth.isAuthenticated().then((value) => {
      if(value){
        loadUserRoles();
      }
    })
    return () =>{}
  }, []);
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <SideNav
        sideNavItems={sideNavItems}
        handleListItemClick={handleListItemClick}
        selectedLink={selectedLink}
        drawerWidth={drawerWidth}
        drawerOpen={drawerOpen}
        toggleDrawer={toggleDrawer}
        userRoles={userRoles}
      />
      <Main drawerOpen={drawerOpen}>
        <DrawerHeader />
        <Security
          oktaAuth={oktaAuth}
          onAuthRequired={customAuthHandler}
          restoreOriginalUri={restoreOriginalUri}
        >
          <Switch data-testid="test">
            {routes.map((route) => {
              const Component = route.component;
              return (
                <SecureRoute
                  path={route.path}
                  key={route.path}
                  component={() => (
                    <BasicLayout
                      drawerOpen={drawerOpen}
                      drawerWidth={drawerWidth}
                      toggleDrawer={toggleDrawer}
                      pageTitle={route.title}
                      Icon={route.Icon}
                    >
                      <Component {...route.props} />
                    </BasicLayout>
                  )}
                />
              );
            })}
            <SecureRoute
              exact
              path={'/'}
              render={() => {
                return <Redirect exact to={'/dashboard'} />;
              }}
            />
            <Route
              path="/login"
              render={() => <Login config={oktaSignInConfig} />}
            />
            <SecureRoute
              path="*"
              exact={true}
              component={() => (
                <PageInProgress
                  drawerWidth={drawerWidth}
                  drawerOpen={drawerOpen}
                  toggleDrawer={toggleDrawer}
                />
              )}
            />
          </Switch>
        </Security>
      </Main>
    </Box>
  );
}

export default App;
