import React, { FC } from 'react';
import {
  Divider,
  Drawer,
  IconButton,
  List,
  Typography,
  useTheme,
} from '@mui/material';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import MenuItemList from './menu-item-list.component';
import { styled } from '@mui/material/styles';
import { SideNavigationItems, TitleProps } from '../shared/interfaces';

interface ISideNav extends TitleProps {
  sideNavItems: SideNavigationItems[];
  handleListItemClick: (path: string, title: string) => void;
  selectedLink: boolean;
  userRoles: string[];
}

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const checkRoles = (
  sideNavItems: SideNavigationItems[],
  userRoles: string[]
): SideNavigationItems[] => {
  return sideNavItems.filter(
    (link) =>
      !link.hasRole ||
      (link.hasRole && link.hasRole.some((role) => userRoles.includes(role)))
  );
};

const SideNav: FC<ISideNav> = ({
  drawerWidth,
  drawerOpen,
  toggleDrawer,
  sideNavItems,
  handleListItemClick,
  selectedLink,
  userRoles,
}) => {
  const theme = useTheme();
  return (
    <>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={drawerOpen}
      >
        <DrawerHeader>
          <Typography variant={'h6'}>Onboarding Experience</Typography>
          <IconButton onClick={toggleDrawer} data-testid="button-drawer">
            {theme.direction === 'ltr' ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List
          sx={{ backgroundColor: '#ffffff' }}
          component={'nav'}
          aria-labelledby="onboarding experience links"
        >
          {checkRoles(sideNavItems, userRoles).map((link, index) => (
            <MenuItemList
              title={link.title}
              elementId={link.elementId}
              path={link.path}
              hasRole={link.hasRole}
              handleListItemClick={handleListItemClick}
              selectedLink={selectedLink}
              key={index}
              Icon={link.Icon}
            />
          ))}
        </List>
      </Drawer>
    </>
  );
};

export default SideNav;
