import Dashboard from '../dashboard/dashboard.component';
import PriorityQueueAssignments from '../onboarding-queue/assignments/onboarding-queue-assignment.component';
import { FC } from 'react';
import IconDashboard from '@mui/icons-material/Dashboard';
import IconPhone from '@mui/icons-material/Call';
import IconBolt from '@mui/icons-material/Bolt';
import IconAssignment from '@mui/icons-material/Assignment';
import IconTask from '@mui/icons-material/Task';
import { TitleProps } from '../shared/interfaces';
import { Role } from '../shared/role.enum';
import CallNextSp from '../onboarding-queue/call-next-service-provider/call-next-service-provider.component';
import PowerHourContainer from '../onboarding-queue/power-hour/power-hour.component';
import PauseAutoAllocation from '../allocation/pause-auto-allocation/pause-auto-allocation.component';

interface IRoute {
  component: FC<TitleProps>;
  path: string;
  props: TitleProps;
  title: string;
  elementId: string;
  hasRole?: string[];
  Icon?: any;
}

export const Routes = (props: TitleProps): IRoute[] => {
  return [
    {
      component: Dashboard,
      path: '/dashboard',
      props: props,
      title: 'Dashboard',
      elementId: 'dashboard',
      Icon: IconDashboard,
    },
    {
      component: CallNextSp,
      path: '/call-next-sp',
      props: props,
      title: 'Call Next Service Provider',
      elementId: 'call-next-sp',
      Icon: IconPhone,
    },
    {
      component: PauseAutoAllocation,
      path: '/pause-auto-allocation',
      props: props,
      title: 'Pause Auto Allocation',
      elementId: 'pause-auto-allocation',
      hasRole: [Role.BccAdmin],
      Icon: IconTask,
    },
    {
      component: PowerHourContainer,
      path: '/power-hour',
      props: props,
      title: 'Power Hour',
      elementId: 'power-hour',
      Icon: IconBolt,
    },
    {
      component: PriorityQueueAssignments,
      path: '/assignments',
      props: props,
      title: 'Assignments',
      elementId: 'priority-queue-assignments',
      hasRole: [Role.BccAdmin],
      Icon: IconAssignment,
    },
  ];
};
