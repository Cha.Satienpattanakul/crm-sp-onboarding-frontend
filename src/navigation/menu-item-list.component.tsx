import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';
import { createStyles, makeStyles } from '@mui/styles';
import { List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { SideNavigationItems } from '../shared/interfaces';

interface IMenuItemList extends SideNavigationItems {
  handleListItemClick: (path: string, title: string) => void;
  selectedLink: boolean;
}

const useStyles = makeStyles((theme) =>
  createStyles({
    menuItem: {
      backgroundColor: '#ffffff',
      '&.selected': {
        backgroundColor: 'transparent',
        background: 'rgba(0, 0, 0, 0.08)',
      },
      '&.active': {
        background: 'rgba(0, 0, 0, 0.08)',
        backgroundColor: '#ffffff',
        '& .MuiListItemIcon-root': {
          color: '#1e3a8a',
        },
        '& .MuiListItemText-primary': {
          fontWeight: 'normal',
          fontStyle: 'italic',
        },
      },
      '& .MuiListItemText-primary': {
        fontWeight: 'bold',
        fontSize: 17,
      },
    },
  })
);

const MenuItemList: FC<IMenuItemList> = ({
  title,
  elementId,
  path,
  handleListItemClick,
  selectedLink,
  Icon,
}) => {
  const classes = useStyles();
  return (
    <>
      <ListItem
        button
        component={NavLink}
        to={`${path}`}
        id={elementId}
        key={elementId}
        selected={selectedLink}
        onClick={() => handleListItemClick(path, title)}
        className={classes.menuItem}
        sx={{ backgroundColor: '#ffffff' }}
      >
        <ListItemIcon sx={{ minWidth: '32px', color: '#3b82f6' }}>
          <Icon />
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItem>
    </>
  );
};

export default MenuItemList;
