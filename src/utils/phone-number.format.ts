export const formattedPhoneNumber = (phoneNumber: string) => {
    if(phoneNumber) {
        const parsedNumber = phoneNumber.replace(/-/g, '');
        return `(${parsedNumber.slice(0, 3)}) ${parsedNumber.slice(3, 6)}-${parsedNumber.slice(6)}`;
    } else {
        return '';
    }
};
